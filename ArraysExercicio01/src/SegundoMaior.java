import java.util.Scanner;

public class SegundoMaior {

	public static void main(String[] args) {

		int maior;
		int segundo_maior;
		int tamanho;

		Scanner s = new Scanner(System.in);

		System.out.println("Digite o tamanho do array");
		tamanho = s.nextInt();

		int vetor[] = new int[tamanho];

		// preenche o array
		for (int i = 0; i < vetor.length; i++) {
			System.out.println("Digite o valor " + i + " do array");
			vetor[i] = s.nextInt();
		}
		
		
		//atribui um valor inicial a maior e segundo maior
		maior = vetor[0];
		segundo_maior = vetor[0];
		
		//procura o maior e o segundo maior

		for (int i = 0; i < vetor.length; i++) {
			if (vetor[i] > maior) {
				segundo_maior = maior;
				maior = vetor[i];
			} else if (vetor[i] > segundo_maior && segundo_maior < maior) {
				segundo_maior = vetor[i];
			}
		}
		
		//exibe o array
		System.out.print("Array = [ ");
		for (int i = 0; i < vetor.length; i++) {
			System.out.print(vetor[i] + " ");
		}
		System.out.print("]");

		System.out.println("\nO segundo maior elemento é " + segundo_maior);

	}

}
