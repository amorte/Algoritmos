#include <stdio.h>
#include <math.h>
#include <stdlib.h>

int verifica_par(int numero){
	if (numero % 2 == 0) {
	   return 1;
	}

	return 0;
}

void carregar_e_ler_vetor(){
	int vetor[100];

    //Preenche o vetor
	for (int i = 0; i < 100; ++i)
	{
		vetor[i] = i;
	}

	//imprime o vetor

	for (int i = 0; i < 100; ++i)
	{
		printf("Posição %d  = %d \n" , i, vetor[i]);
	}

}

int calculaPotencia(valor1, valor2){
	//Para usar a função pow devemos impotar a biblioteca math.h
	return pow(valor1,valor2);
}

int maiorElemento(int vetor[]){
	int maior = vetor[0];

	for (int i = 0; i < 10; ++i)
	{
		if (vetor[i] > maior) {
			maior = vetor[i];
		}
	}

	return maior;
}


int main(){
	int i;
	printf("Digite um valor \n" );
	scanf("%d", &i);

	int resultado = verifica_par(i);

	if (resultado == 0) {
		printf("O número %d é ímpar \n", i );
	}else {
		printf("O número %d é par \n", i );
	}

	//carregar_e_ler_vetor();

	int array[10];

	for (int i = 0; i < 10; ++i)
	{
		array[i] = i;
	}

	int maiorValor = maiorElemento(array);

	printf("O maior elemento é %d \n" , maiorValor  );

	return 0;

}
