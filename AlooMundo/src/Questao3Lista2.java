import java.util.Scanner;

public class Questao3Lista2 {
	
	public static void main(String[] args) {
		int valor1;
		int valor2;
		Scanner s = new Scanner(System.in);
		
		System.out.println("Digite o valor 1");
		valor1 = s.nextInt();
		System.out.println("Digite o valor 2");
		valor2 = s.nextInt();
		
		System.out.println("O quociente é " + (valor1 / valor2));
		System.out.println("O resto é " + (valor1 % valor2));
	}

}
