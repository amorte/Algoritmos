import java.util.Scanner;

public class CalculoMulta {
	
	public static void main(String[] args) {
		double velocidadeVia;
		double velocidadeAferida;
		double diferenca; 
		
		Scanner s = new Scanner(System.in);
		
		System.out.println("Digite a velocidade permitida da via");
		velocidadeVia = s.nextDouble();
		System.out.println("Digite a velocidade aferida");
		velocidadeAferida = s.nextDouble();
		
		diferenca = (velocidadeAferida - velocidadeVia);
		
		if(diferenca <= 0) {
			System.out.println("Dentro da velocidade permitida");
		} else if (diferenca > 0 && diferenca <= 10) {
			System.out.println("Voce ultrapassou a velocidade em " + 
		diferenca + " Km/h. Multa de R$ 50");
			
		} else if (diferenca > 10 && diferenca <=30) {
			System.out.println("Voce ultrapassou a velocidade em " + diferenca + " Km/h. Multa de R$ 100");
		} else {
			System.out.println("Voce ultrapassou a velocidade em " + diferenca + " Km/h. Multa de R$ 200");
		}
		
		
	}

}
