import java.util.Scanner;

public class ExemploCigarros {
	
	public static void main(String[] args) {
		int quantidadeCigarroDia;
		int quantidadeAnos;
		double precoCarteira;
		
		double totalCigarros;
		double totalCarteiras;
		double valorFinal;
		Scanner s = new Scanner(System.in);
		
		System.out.println("Quantos cigarros vc fuma por dia?");
		quantidadeCigarroDia = s.nextInt();
		
		System.out.println("A quantos anos vc fuma?");
		quantidadeAnos = s.nextInt();
		
		System.out.println("Qual o preço da carteira?");
		precoCarteira = s.nextDouble();
		
		totalCigarros = quantidadeCigarroDia * 30 * 12 * quantidadeAnos;
		
		totalCarteiras = totalCigarros / 20;
		
		valorFinal = totalCarteiras * precoCarteira;
		
		System.out.println("Seu gasto total foi de " + valorFinal);
		
	}

}
