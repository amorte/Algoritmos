import java.util.Scanner;

public class Notas {
	
	public static void main(String[] args) {
		double notaAp1;
		double notaAp2;
		double notaAp3;
		double media;
		Scanner s = new Scanner(System.in);
		
		System.out.println("Digite a nota da AP1");
		notaAp1 = s.nextDouble();
		
		System.out.println("Digite a nota da AP2");
		notaAp2 = s.nextDouble();
		
		System.out.println("Digite a nota da AP3");
		notaAp3 = s.nextDouble();
		
		media = (notaAp1 * 0.3 + notaAp2 * 0.3 + notaAp3 * 0.4);
		
		if (media >= 5) {
			System.out.println("Aprovado");
		}else {
			System.out.println("reprovado");
		}
	}

}
