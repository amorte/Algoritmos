import java.util.Scanner;

public class Tabuada {
	
	public static void main(String[] args) {
		int numero ;
		Scanner scanner = new Scanner(System.in);
		int cont = 0;
		
		System.out.println("Digite um número para exibirmos a tabuada");
		numero = scanner.nextInt();
		
		System.out.println("======Tabuada de " + numero);
		while(cont <=10){
			System.out.println(numero + " x " + cont + " = " + (numero * cont));
			//System.out.println(cont);
			cont++;
		}
		
		

	}

}
