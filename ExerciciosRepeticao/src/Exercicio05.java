import java.util.Scanner;

public class Exercicio05 {
	
	public static void main(String[] args) {
		int numero;
		Scanner s = new Scanner(System.in);
		
		System.out.println("Digite um número");
		numero = s.nextInt();
		
		int contador = 0;
		
		while (contador <= 10) {
			System.out.println(numero + " x " 
		     + contador + " = " + (numero*contador)  );
			contador++;
		}
		
		for (int i = 0; i <= 10; i++) {
			System.out.println(numero + " x " 
				     + i + " = " + (numero*i)  );
		}
	}

}
