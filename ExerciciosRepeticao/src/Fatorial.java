import java.util.Scanner;

public class Fatorial {
	
	public static void main(String[] args) {
		int fat = 1;
		int numero;
		Scanner scanner = new Scanner(System.in);
		
		System.out.println("Digite um número para calcular o fatorial");
		
		numero = scanner.nextInt();

//        for (int i = numero; i > 1; i--) {
//                fat = fat * i;
//        }
		int cont = numero;
		while (cont != 1) {
			fat = fat * cont;
			System.out.println(fat);
			//cont--;
		}
        
        System.out.println("O fatorial de " + numero + " é: " + fat);
	}

}
