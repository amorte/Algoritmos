import java.util.Scanner;

/*
 * Calcular a quantidade dinheiro gasta por um fumante. 
 * Dados: o número de anos que ele fuma, o nº de cigarros fumados por dia e o
 *  preço de uma carteira com 20 cigarros. 
 */
public class Exercicio01 {
	
	public static void main(String[] args) {
		int qtdAnos;
		int qtdCigarrosDia;
		double precoCarteira;
		Scanner s = new Scanner(System.in);
		
		System.out.println("Digite a quantidade de anos que você fuma");
		qtdAnos = s.nextInt();
		System.out.println("Digita a quantidade de cigarros por dia");
		qtdCigarrosDia = s.nextInt();
		System.out.println("Digite o preço da carteira");
		precoCarteira = s.nextDouble();
		
		double valorFinal = (qtdCigarrosDia * 365 * qtdAnos * precoCarteira) / 20;
		
		System.out.println("Em " + qtdAnos + " você gastou R$ " + valorFinal );
		
	}

}
