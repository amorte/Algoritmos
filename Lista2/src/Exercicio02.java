import java.util.Scanner;

/*
 * Que informe a área e o volume de um cilindro. 
 * Utilize a diretiva static final para armazenar o valor de π (3.14159).  
 * 
 */
public class Exercicio02 {
	
	public static void main(String[] args) {
		 final double PI = 3.14159;
		 
		 double raio;
		 double altura;
		 
		 Scanner s = new Scanner(System.in);
		 
		 System.out.println("Digite o valor do raio");
		 raio = s.nextDouble();
		 System.out.println("Digite o valor da altura");
		 altura = s.nextDouble();
		 
		 
		 System.out.println("A área é = " + (2*PI*raio)*(raio+altura));
		 
		 System.out.println("O volume é = " + (PI * raio * raio * altura));
		
	}

}
