import java.util.Scanner;


/*
 * Para ler 3 números reais do teclado e verificar se o primeiro é maior que a soma dos outros dois.

 */
public class Exercicio04 {
	
	public static void main(String[] args) {
		Scanner s = new Scanner(System.in);
		
		double valor1;
		double valor2;
		double valor3;

		
		System.out.println("Digite o valor 1");
		valor1 = s.nextDouble();

		System.out.println("Digite o valor 2");
		valor2 = s.nextDouble();
		
		System.out.println("Digite o valor 3");
		valor3 = s.nextDouble();
		
		if (valor1 > (valor2 + valor3)) {
			System.out.println("Valor 1 é maior que a soma dos outros dois valores");
		} else {
			System.out.println("Valor 1 não é maior que a soma dos outros dois valores");
		}
	}

}
