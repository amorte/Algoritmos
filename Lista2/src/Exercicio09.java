import java.util.Scanner;

/*
 * Que leia dois valores e realize uma das operações abaixo descritas abaixo. O programa deve 
 * ser feito usando a estrutura de controle “switch...case”, possibilitando a escolha do usuário 
 * por uma das opções. (Soma, subtração, multiplicação, divisão e comparação 
 * (informando se eles são iguais ou, no caso de diferentes, o maior entre eles)).
 */
public class Exercicio09 {
	
	public static void main(String[] args) {
		int valor1;
		int valor2;
		Scanner s = new Scanner(System.in);
		
		System.out.println("Digite o valor 1");
		valor1 = s.nextInt();
		System.out.println("Digite o valor 2");
		valor2 = s.nextInt();
		
		System.out.println("Digite uma das opções.\n1-Soma\n2-Subtração\n3-Multiplicação\n4-Divisão\n5-Comparação");
		int opcao = s.nextInt();
		
		switch (opcao) {
		case 1: 
			System.out.println("A soma é = " + (valor1 + valor2));
			break;
		case 2:
			System.out.println("A subtração é = " + (valor1 - valor2));
			break;
		case 3:
			System.out.println("A multiplicação é = " + (valor1 * valor2));
			break;
		case 4:
			if (valor2 != 0) {
				System.out.println("A divisão é = " + (valor1 / valor2));
			} else {
				System.out.println("Divisão por zero");
			}
			break;
		case 5:
			if (valor1 == valor2) {
				System.out.println("Os valores são iguais");
			} else if (valor1 > valor2) {
				System.out.println("Valor "+ valor1 + " é maior");
			} else {
				System.out.println("Valor " + valor2 + " é maior");
			}
			break;
		default:
			System.out.println("Opção inválida.");
		}
		
	}

}
