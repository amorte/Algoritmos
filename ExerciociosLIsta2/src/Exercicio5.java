import java.util.Scanner;

public class Exercicio5 {
	
	public static void main(String[] args) {
		
		double velocidadeAtual;
		double velocidadePermitida;
		double valorDaMulta = 0;
		
		Scanner s = new Scanner(System.in);
		
		System.out.println("Digite a velocidade atual");
		velocidadeAtual = s.nextDouble();
		
		System.out.println("Digite a velocidade permitida");
		velocidadePermitida = s.nextDouble();
		
		double diferenca = velocidadeAtual - velocidadePermitida;
		
		if (diferenca > 0 && diferenca <= 10){
			valorDaMulta = 50;
		} else if (diferenca > 10 && diferenca <= 30){
			valorDaMulta = 100;
		} else{
			valorDaMulta = 200;
		}
		
		
		if (diferenca > 0) {
			System.out.println("Voce ultrapassou a velocidade permitida em " + diferenca + " Km. Pagará multa de R$ " + valorDaMulta);
		}else {
			System.out.println("Parabéns. Você não ultrapassou a velocidade permitida");
		}
	}

}
