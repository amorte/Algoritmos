import java.util.Scanner;


public class Calc {
	
	static Scanner s = new Scanner(System.in);
	
	public static void main(String[] args) {
		int valor1;
		int valor2;
		int operacao;
		
		System.out.println("Digite valor 1");
		valor1 = s.nextInt();
		System.out.println("Digite valor 2");
		valor2 = s.nextInt();
		
		System.out.println("Escolha uma operação  \n1 - Soma \n2 - Subtração \n3 - "
				+ "Multiplicação \n4 - Divisão \n5 - Fatorial \n6 - Potência \n7- Equação (N! * nˆm)/ n");
		operacao = s.nextInt();
		
		switch(operacao) {
		case 1:
			int resultado = soma(valor1, valor2);
			System.out.println("A soma é " + resultado);
			break;
		case 2:
			subtracao(valor1, valor2);
			break;
		case 3:
			System.out.println("A soma é " + multiplicacao(valor1, valor2));
			break;
		case 4:
			divisao(valor1, valor2);
			break;
		case 5:
			int fat = fatorial(valor1);
			System.out.println("O fatorial é " + fat);
			break;
		case 6:
			System.out.println("Potência = " + potencia (valor1, valor2));
			break;
		case 7: 
			double equacao = (fatorial(valor1) * potencia(valor1, valor2))/valor1;
			break;
		default:
			System.out.println("Operação inválida");
		}
		
		
	}
	
	static int soma(int num1, int num2) {
		int resultado = num1 + num2;
		
		return resultado;
	}
	
	static void subtracao (int valor1, int valor2) {
	
		System.out.println("Resultado = " + (valor1 - valor2));
		
	}
	
	static int multiplicacao(int valor1, int valor2) {
		return valor1 * valor2;
	}
	
	static void divisao(int valor1, int valor2) {
		if (valor2 == 0) {
			System.out.println("Divisão por zero");
		} else {
			System.out.println("Resultado = " + (valor1/valor2));
		}
		
	}
	
	
	
	static int fatorial(int numero) {
		if (numero == 0) {
			return 1;
		}
		int fat = 1;
		for (int i = numero; i > 1; i--) {
			fat = fat * i;
		}
		return fat;
	}
	
	static double potencia(int valor1, int valor2){
		if (valor2 == 0){
			return 1;
		} else if (valor2 == 1) {
			return valor1;
		} else {
			int result = valor1;
			for (int i = 1; i < valor2; i++) {
				result *= valor1;
			}
			return result;
		}
		
	}
	
	
	

}
