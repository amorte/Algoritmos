import java.util.Scanner;

public class FuncaoBasico {
	
	static int valor = 5;
	
	public static void main(String[] args) {
		int num1;
		int num2;
		Scanner s = new Scanner(System.in);
		
		System.out.println("Digite o numero 1");
		num1 = s.nextInt();
		System.out.println("Digite o numero 2");
		num2 = s.nextInt();
		
		int resultado = adicao(num1, num2);
		
		valor++;
		
		subtracao(num1, num2);
		
		System.out.println("A soma é " + resultado);
	}
	
	static int adicao(int numero1, int numero2){
		int resultado = numero1 + numero2;
		return resultado;
	}
	
	static int adicao(int numero1, int numero2, int numero3){
		int resultado = numero1 + numero2;
		return resultado;
	}
	
	static int subtracao(int numero1, int numero2) {
		return numero1 - numero2;
	}
	
	static void multiplicacao (int numero1, int numero2){
		System.out.println("Resultado = " + (numero1 * numero2));
	}

}
