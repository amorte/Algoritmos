import java.util.Scanner;

import javax.swing.JOptionPane;

public class Exemplo01 {
	static int valor;
	
	static int potencia (int base, int expoente){
		int potencia = base;
		int cont = 1;
		while (cont < expoente){
			potencia *= base;
			cont++;
		}
		
		return potencia;
		
	}
	
	public static void main(String[] args) {
		int valores[] = new int[100];
		Scanner s = new Scanner(System.in);
		
		for (int i = 0; i < valores.length; i++) {
			System.out.println("Digite um valor");
			valores[i] = s.nextInt();
		}
		
	    int potencia = potencia(3, 3);
		int maior_par = maiorPar(valores);
	}
	
	static int maiorPar(int [] array){
		int maior_par = 0;
		for (int i = 0; i < array.length; i++) {
			if (array[i] % 2 == 0 && 
					array[i] > maior_par){
				maior_par = array[i];
			}
		}
		return maior_par;
	}
	
	
	
	static void soma(){
		
	}

}
