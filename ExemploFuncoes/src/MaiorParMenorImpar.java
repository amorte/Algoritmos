import java.util.Scanner;

public class MaiorParMenorImpar {
	
	public static void main(String[] args) {
		int maiorPar = 0;
		int menorImpar = 0;
		int somatorio = 0;
		double media;
		Scanner s = new Scanner(System.in);
		
		int valores [] = new int [5];
		
		//Preenche o array
		for (int i = 0; i < valores.length; i++) {
			System.out.println("Digite um valor");
			valores[i] = s.nextInt();
			
			if (valores[i] % 2 == 0 ){ 
				maiorPar = valores[i];
			} else {
				menorImpar = valores[i];
			}
		}
		
		//Varre o array
		for (int i = 0; i < valores.length; i++) {
			if (valores[i] % 2 == 0 && valores[i] > maiorPar) {
				maiorPar = valores[i];
			} else if(valores[i] % 2 != 0 && valores[i] < menorImpar){
				menorImpar = valores[i];
			}
			
			somatorio = somatorio + valores[i];
		}
		
		media = somatorio / valores.length;
		
		if (maiorPar == 0) {
			System.out.println("Não existe valor par");
		} else {
			System.out.println("Maior par = " + maiorPar);
		}
		
		if (menorImpar == 0) {
			System.out.println("Não existe valor impar");
		} else {
			System.out.println("Menor impar = " + menorImpar);
		}
		
		System.out.println("Somatório = " + somatorio);
		System.out.println("Média = " + media);
		
	}

}
