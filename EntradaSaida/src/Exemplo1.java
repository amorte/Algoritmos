import java.util.Scanner;

public class Exemplo1 {
	
	public static void main(String[] args) {
		double numero1;
		double numero2;
		double resultado;
		
		Scanner s = new Scanner(System.in);
		
		System.out.println("Digite um valor real");
		numero1 = s.nextDouble();
		
		System.out.println("Digite outro valor real");
		numero2 = s.nextDouble();
		
		resultado = numero1 + numero2;
		
		System.out.println("O resultado é: " + resultado);
		
	}

}
